import { browser, protractor, by, element, ElementFinder } from 'protractor';
import { HomePageObject } from './home.po';

export class LoginPageObject {

  homePage = new HomePageObject();

  navigateTo() {
    this.homePage.navigateTo();
    this.homePage.getLogoutButton().click();
    browser.wait(protractor.ExpectedConditions.not((protractor.ExpectedConditions.urlContains('home'))));
  }

  getKeyInput() {
    return element(by.deepCss('.key-input input'));
  }

  getLoginButton() {
    return element(by.deepCss('.login-button'));
  }

}