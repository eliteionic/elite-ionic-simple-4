import { browser, by, element, ElementFinder } from 'protractor';
import { HomePageObject } from './home.po';

export class LessonSelectPageObject {

  homePage = new HomePageObject();

  navigateTo() {
    this.homePage.navigateTo();
    return this.homePage.getModuleListItems().first().click();
  }

  getLessonListItems(){
    return element.all(by.deepCss('.lesson-list ion-item'));
  }

}