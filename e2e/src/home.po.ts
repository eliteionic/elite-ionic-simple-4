import { protractor, browser, by, element, ElementFinder } from 'protractor';

export class HomePageObject {

  navigateTo() {

    browser.get('/');

    browser.wait(protractor.ExpectedConditions.urlContains('home'));

  }

  getModuleListItems() {
    return element.all(by.deepCss('.module-list ion-item'));
  }

  getLogoutButton(){
    return element(by.deepCss('.logout-button'));
  }

}