import { LessonSelectPageObject } from './lesson-select.po';
import { LessonPageObject } from './lesson.po';

describe('Lesson Select', () => {

    let lessonSelectPage: LessonSelectPageObject;
    let lessonPage: LessonPageObject;

	beforeEach(() => {
		
        lessonSelectPage = new LessonSelectPageObject();
        lessonPage = new LessonPageObject();
		lessonSelectPage.navigateTo();

	});

	it('the list of lessons should contain the titles of the lessons', () => {
	
		expect(lessonSelectPage.getLessonListItems().first().getText()).toContain('lesson 1');

	});

	it('after selecting a specific lesson, the user should be able to see content for that lesson', () => {

		lessonSelectPage.getLessonListItems().first().click();

		expect(lessonPage.getLessonContent().getText()).toContain('this is the lesson content');

	});

});