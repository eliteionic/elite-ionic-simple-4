import { browser, protractor } from 'protractor';
import { HomePageObject } from './home.po';
import { LoginPageObject } from './login.po';
import { LessonSelectPageObject } from './lesson-select.po';

describe('Home', () => {

	let homePage: HomePageObject;
	let loginPage: LoginPageObject;
    let lessonSelectPage: LessonSelectPageObject;

	beforeEach(() => {
		
		homePage = new HomePageObject();
		loginPage = new LoginPageObject();
        lessonSelectPage = new LessonSelectPageObject();
		homePage.navigateTo();

	});

	it('should be able to view a list of modules', () => {

		expect(homePage.getModuleListItems().count()).toBe(5);

	});

	it('the list of modules should contain the titles of the modules', () => {
	
		expect(homePage.getModuleListItems().first().getText()).toContain('Module One');

	});

	it('after selecting a specific module, the user should be able to see a list of available lessons', () => {
	
		let moduleToTest = homePage.getModuleListItems().first();

		moduleToTest.click();

		expect(lessonSelectPage.getLessonListItems().count()).toBeGreaterThan(0);

	});

    it('should be able to log out and back in', () => {

        homePage.getLogoutButton().click();

        browser.wait(protractor.ExpectedConditions.not((protractor.ExpectedConditions.urlContains('home'))));

        /* Log back in to prevent rest of tests breaking */

        let input = loginPage.getKeyInput();
        let loginButton = loginPage.getLoginButton();

        input.sendKeys('abcd-egfh-ijkl-mnop');

        loginButton.click();

        browser.wait(protractor.ExpectedConditions.urlContains('home'));        

    });

});