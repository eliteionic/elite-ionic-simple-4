import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { NavController } from '@ionic/angular';

import { HomePage } from './home.page';
import { AuthService } from '../services/auth.service';
import { ModulesService } from '../services/modules.service';
import { ModulesMock } from '../../../mocks/mocks-app';
import { AuthMock } from '../../../mocks/mocks-app';
import { NavMock } from '../../../mocks/mocks-ionic';

describe('HomePage', () => {
  let component: any;
  let fixture: ComponentFixture<HomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePage ],
      providers: [
        { provide: AuthService, useClass: AuthMock },
        { provide: ModulesService, useClass: ModulesMock },
        { provide: NavController, useClass: NavMock }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component instanceof HomePage).toBeTruthy();
  });

  it('should have a class member called modules that is an array', () => {

    expect(component.modules instanceof Array).toBe(true);

  });

  it('the modules class member should contain 5 modules after ngOnInit has been triggered', () => {

    let moduleProvider = fixture.debugElement.injector.get(ModulesService);

    spyOn(moduleProvider, 'getModules').and.returnValue([{},{},{},{},{}]);

    component.ngOnInit();

    expect(component.modules.length).toBe(5);

  });

  it('the openModule() function should navigate to the LessonListPage', () => {

    let navCtrl = fixture.debugElement.injector.get(NavController);

    spyOn(navCtrl, 'navigateForward');

    let testModule = {title: 'pretend module', id: '1'};

    component.openModule(testModule.id);

    expect(navCtrl.navigateForward).toHaveBeenCalledWith('/module/' + testModule.id);

  });

  it('the logout function should call the logout method of the auth provider', () => {

    let authProvider = fixture.debugElement.injector.get(AuthService);
  
    spyOn(authProvider, 'logout');

    component.logout();

    expect(authProvider.logout).toHaveBeenCalled();

  });

});