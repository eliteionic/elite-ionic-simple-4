import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';

import { LessonSelectPage } from './lesson-select.page';
import { NavController } from '@ionic/angular';

import { ModulesService } from '../services/modules.service';

import { NavMock } from '../../../mocks/mocks-ionic';
import { ModulesMock } from '../../../mocks/mocks-app';

describe('LessonSelectPage', () => {
  let component: any;
  let fixture: ComponentFixture<LessonSelectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LessonSelectPage ],
      providers: [
        {
          provide: ActivatedRoute, useValue: {
            snapshot: { paramMap: { get: (path) => { return '1'} }}
          }
        },
        { provide: NavController, useClass: NavMock },
        { provide: ModulesService, useClass: ModulesMock }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonSelectPage);
    component = fixture.componentInstance;
  });

  it('should create component', () => {
    expect(component instanceof LessonSelectPage).toBe(true);
  });

  it('should have a module class member that is an object', () => {
    expect(component.module instanceof Object).toBe(true);
  });

  it('should set up the passed in module on the module class member', () => {

    let modulesService: any = fixture.debugElement.injector.get(ModulesService);

    modulesService.getModuleById = jasmine.createSpy('getModuleById').and.returnValue({
      id: '1',
      title: 'test',
      description: 'test',
      lessons: [
        {title: 'lesson 1'},
        {title: 'lesson 2'},
        {title: 'lesson 3'},
        {title: 'lesson 4'}
      ]
    });

    component.ngOnInit();

    expect(component.module.lessons.length).toBe(4);

  });

  it('the openLesson() function should navigate to the LessonPage', () => {

    let navCtrl = fixture.debugElement.injector.get(NavController);

    spyOn(navCtrl, 'navigateForward');

    component.module = {
      id: '1'
    };

    let testLesson = {id: '1'};

    component.openLesson(testLesson);

    expect(navCtrl.navigateForward).toHaveBeenCalledWith('/module/1/lesson/' + testLesson.id);

  });

});