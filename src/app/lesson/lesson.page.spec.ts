import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';

import { ModulesService } from '../services/modules.service';
import { ModulesMock } from '../../../mocks/mocks-app';

import { LessonPage } from './lesson.page';

describe('LessonPage', () => {
  let component: any;
  let fixture: ComponentFixture<LessonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LessonPage ],
      providers: [
        {
          provide: ActivatedRoute, useValue: {
            snapshot: { paramMap: { get: (path) => { return '1'} }}
          }
        },
        { provide: ModulesService, useClass: ModulesMock }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonPage);
    component = fixture.componentInstance;
  });

  it('should create component', () => {
    expect(component instanceof LessonPage).toBeTruthy();
  });

  it('should have a lesson class member that is an object', () => {
    expect(component.lesson instanceof Object).toBe(true);
  });

  it('should set up the passed in lesson as the lesson class member', () => {

    let modulesService: any = fixture.debugElement.injector.get(ModulesService);

    modulesService.getLessonById = jasmine.createSpy('getLessonById').and.returnValue({
        title: 'lesson 1',
        content: 'this is the test content'
    });

    component.ngOnInit();

    expect(component.lesson.title).toBe('lesson 1');

  });

});