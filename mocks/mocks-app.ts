export class ModulesMock {
 
    public getModules(): Object[] {
      return [{}]
    }
   
    public getModuleById(id): Object {
      return {}
    }
  
}

export class AuthMock {

  public checkKey(key: string): any {}  

  public reauthenticate(): any {} 

  public logout(): any {}

}